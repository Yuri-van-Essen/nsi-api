# Nederlands Slaap Instituut • [nederlandsslaapinstituut.nl](https://www.nederlandsslaapinstituut.nl/)

##  **Patient registration**

### Document version 1.1


##### **18-05-2020**

The current API is offered by [Asterisque](https://asterisque.nl/) and called via an AJAX call in WordPress.

API URL:
```
https://asterisqueapi.asterisque.nl/api/
```

The PHP function is written in:

```
functions.php, line 99
```
of the NSI WordPress theme.

The Javascript function is in:

```
~/wp-content/themes/NSI/js/nsi-forms.js, line 99. 
```

The minified version is used by WordPress:
```
functions.php, line 417
```

On the basis of the API URL and authorization supplied by Asterisque, after completing the form with the correct data(an extra check is made on the BSN number), whether the API post is valid. The data is then posted to Asterisque for further processing.
After the API post, a confirmation email is sent to the patient and NSI. The entered data is also stored locally in the database for NSI' own insight.

The 'Mijn NSI' section is fully managed by Asterisque.

### By

* *[VHRL](https://www.vrhl.nl)*